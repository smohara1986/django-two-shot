from django.urls import path
from receipts.views import (
    ListView_Receipt,
    CreateView_Receipt,
    ListView_Accounts,
    ListView_ExpenseCategory,
    CreateView_ExpenseCategory,
    CreateView_Account,
)


urlpatterns = [
    path("", ListView_Receipt, name="home"),
    path("create/", CreateView_Receipt, name="create_receipt"),
    path("categories/", ListView_ExpenseCategory, name="category_list"),
    path("accounts/", ListView_Accounts, name="account_list"),
    path(
        "categories/create/",
        CreateView_ExpenseCategory,
        name="create_category",
    ),
    path("accounts/create/", CreateView_Account, name="create_account"),
]
