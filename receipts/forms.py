from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

    def __init__(self, user, *args, **kwargs):
        super(CreateReceiptForm, self).__init__(*args, **kwargs)
        self.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=user
        )
        self.fields["account"].queryset = Account.objects.filter(owner=user)


class CreateExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class CreateAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
