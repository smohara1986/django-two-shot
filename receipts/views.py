from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateAccountForm,
    CreateExpenseCategoryForm,
)


# Create your views here.


@login_required
def ListView_Receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_variable": receipt_list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def CreateView_Receipt(request):
    if request.method == "GET":
        form = CreateReceiptForm(request.user)
    else:
        form = CreateReceiptForm(request.user, request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def ListView_ExpenseCategory(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_variable": category_list,
    }
    return render(request, "categories/list.html", context)


@login_required
def ListView_Accounts(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_variable": account_list,
    }
    return render(request, "accounts/list.html", context)


@login_required
def CreateView_ExpenseCategory(request):
    if request.method == "GET":
        form = CreateExpenseCategoryForm()
    else:
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def CreateView_Account(request):
    if request.method == "GET":
        form = CreateAccountForm()
    else:
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
